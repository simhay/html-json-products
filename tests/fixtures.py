import io

PRODUCTS_FILE = "tests/_products.html"
PRODUCT_FILE = "tests/_product.html"

with io.open(PRODUCTS_FILE, 'r', encoding='utf-8') as f:
    PRODUCTS_RESPONSE = f.read()

with io.open(PRODUCT_FILE, 'r', encoding='utf-8') as f:
    PRODUCT_RESPONSE = f.read()

PRODUCT_DIR = "http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/"
PRODUCT_LINKS = [
    u"{}sainsburys-apricot-ripe---ready-320g.html".format(PRODUCT_DIR),
    u"{}sainsburys-avocado-xl-pinkerton-loose-300g.html".format(PRODUCT_DIR),
    u"{}sainsburys-avocado--ripe---ready-x2.html".format(PRODUCT_DIR),
    u"{}sainsburys-avocados--ripe---ready-x4.html".format(PRODUCT_DIR),
    u"{}sainsburys-conference-pears--ripe---ready-x4-%28minimum%29.html".format(PRODUCT_DIR),
    u"{}sainsburys-golden-kiwi--taste-the-difference-x4-685641-p-44.html".format(PRODUCT_DIR),
    u"{}sainsburys-kiwi-fruit--ripe---ready-x4.html".format(PRODUCT_DIR)
]
