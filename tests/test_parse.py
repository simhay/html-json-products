from decimal import Decimal

from parse import product_listings, product_detail, extract_price
from products import Product
from .fixtures import PRODUCT_LINKS, PRODUCTS_RESPONSE, PRODUCT_RESPONSE


class TestListings(object):
    def test_extract_product_listings(self):
        assert PRODUCT_LINKS ==  product_listings(PRODUCTS_RESPONSE)

class TestProducts(object):
    def test_extract_price(self):
        assert Decimal('3.50') == extract_price(u"\xa33.50/unit")

    def test_extract_price_non_unit(self):
        assert Decimal('2.55') == extract_price(u"\xa32.55")

    def test_extract_price_non_spaces(self):
        assert Decimal('3.50') == extract_price(u"\xa3 3.50")

    def test_extract_product_detail(self):
        expected = {'title': u"Sainsbury's Apricot Ripe & Ready x5",
                    'unit_price': Decimal('3.50'),
                    'description': u"Apricots"}

        result = product_detail(PRODUCT_RESPONSE)

        for key in expected:
            assert expected[key] == result[key]
