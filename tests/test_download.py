import sys

import pytest
import mock
import requests

from download import get


def get_failure(url):
    class ResponseError(object):
        status_code = 500
    return ResponseError()

def get_success(url):
    class ResponseOK(object):
        status_code = 200
        content = "Body content"
    return ResponseOK()


def test_download_non_200():
    with mock.patch('download.requests.get', get_failure):
        with pytest.raises(requests.exceptions.HTTPError):
            get("http://localhost:8080")

def test_download_200():
    with mock.patch('download.requests.get', get_success):
        assert "Body content" == get("http://localhost:8080")
