from decimal import Decimal

from products import Product

def test_creation():
    expected = {"title": u"Title",
                "size": 3267,
                "unit_price": Decimal("1.80"),
                "description": u"This is a short description of the product"}

    test_product = Product(**expected)
    for key in expected:
        assert getattr(test_product, key) == expected[key]


def test_size_in_kb():
    test_product = Product()
    test_product.set_size_in_kb(3267)
    expected = "3.19kb"
    assert test_product.size == expected

