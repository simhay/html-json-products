from decimal import Decimal


class Product(object):
    """
    Product object for listed item
    """
    def __init__(self, title=u"", size=0, unit_price=Decimal(0), description=u""):
        self.title = title
        self.size = size
        self.unit_price = unit_price
        self.description = description

    def set_size_in_kb(self, length_in_bytes):
        self.size = "{0:.2f}kb".format(length_in_bytes/1024.0)

