.DEFAULT: help
.PHONY: help
help:
	@echo '                                                                       '
	@echo 'Usage:                                                                 '
	@echo '   make test                        test script                        '
	@echo '                                                                       '


.PHONY: test
test:
	py.test -v --cov-report term  --cov=$(CURDIR) tests/
