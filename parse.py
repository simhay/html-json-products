"""
Related functions for extracting out html from source.
"""
from decimal import Decimal
from bs4 import BeautifulSoup


def product_listings(html):
    """
    Extract href links from product listings.

    Links contained within html 'productInfo' class div:

    <div class="productInfo">
        <h3>
        <a href="http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/sainsburys-apricot-ripe---ready-320g.html" >
        Sainsbury's Apricot Ripe & Ready x5
        <img src="http://c2.sainsburys.co.uk/wcsstore7.11.1.161/SainsburysStorefrontAssetStore/wcassets/product_images/media_7572754_M.jpg" alt="" />
        </a>
        </h3>
        ...
    </div>
    """
    soup = BeautifulSoup(html, 'html.parser')
    links = []

    for div in soup.findAll("div", { "class" : "productInfo" }):
        for link in div.find_all('a'):
            links.append(link['href'])

    return links


def extract_price(value):
    """
    Remove newlines and pound sign '\xa3' + trailing '/unit' from original text
    Return Decimal
    """
    return Decimal(value.replace(u"\xa3", u"").replace(u"/unit", u"").strip())


def product_detail(html):
    """
    Extract required product details from page.

    Required elements:
        title
        unit_price
        description
    """
    soup = BeautifulSoup(html, 'html.parser')

    div_title = soup.find("div", { "class" : "productTitleDescriptionContainer" })
    title = div_title.h1.text.strip()

    p_price = soup.find("p", {"class": "pricePerUnit"})
    price = extract_price(p_price.text)

    div_description = soup.findAll("div", { "class" : "productText" })
    description = div_description[0].text.strip()

    return {
        'title': title,
        'unit_price': price,
        'description': description
    }
