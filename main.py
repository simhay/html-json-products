#!/usr/bin/env python
"""
Script to run against a list of product listing url(s).
Outputs to stdout/stderr.

Usage:
    ./main.py < urls.txt 2> error.log

"""
import sys
import requests
from decimal import Decimal

from output import to_json
from parse import product_listings, product_detail
from products import Product
from download import get


def main():
    """
    Iterate over each listing url and gather product details from each page.
    Output results to JSON.
    """
    results = {'results': [], 'total': Decimal(0)}
    for line in sys.stdin.xreadlines():
        product_listing_url = line.rstrip()
        try:
            listing_response = get(product_listing_url)

            # Get product links
            for product_link in product_listings(listing_response):
                # get product
                product_response = get(product_link)

                # Build product
                detail = product_detail(product_response)
                _product = Product(**detail)

                # page body size in kbytes
                _product.set_size_in_kb(len(product_response))

                # Add dictionary to products
                results['results'].append(_product.__dict__)

                # Update total
                results['total'] += _product.unit_price

        except requests.exceptions.HTTPError as e:
            sys.stderr.write('Invalid HTTP request: {}\n'.fomat(e))
            continue

    to_json(results)


if __name__ == "__main__":
    main()
