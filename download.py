import requests

def get(url):
    """
    Attempts to GET url and raises requests exception for any non 200 response.
    Returns content of the response in bytes
    """
    r = requests.get(url)
    if r.status_code != 200:
        msg = 'Non 200 status code returned: {}'.format(r.status_code)
        raise requests.exceptions.HTTPError(msg)

    return r.content


