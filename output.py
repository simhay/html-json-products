import sys
import json
from decimal import Decimal


class DecimalEncoding(json.JSONEncoder):
    def default(self, obj):
        """
        Handle Decimal types and format to string with 2 decimal places.
        No truncation on trailing zero values.
        """
        if isinstance(obj, Decimal):
            return "{0:.2f}".format(obj)
        return json.JSONEncoder.default(self, obj)


def to_json(output):
    " Output to stdout in JSON format "
    sys.stdout.write(json.dumps(output, ensure_ascii=False, cls=DecimalEncoding))
