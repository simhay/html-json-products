# Test

Scrapes the Sainsbury’s grocery site and return a JSON array of all the products on the page.

# Environment

    $ cat /etc/lsb-release
    DISTRIB_ID=Ubuntu
    DISTRIB_RELEASE=14.04
    DISTRIB_CODENAME=trusty
    DISTRIB_DESCRIPTION="Ubuntu 14.04.3 LTS"

    $ python -V
    Python 2.7.6


# Installation

## Virtual env set up

Create a virtualenv and install pip dependencies.

    $ virtualenv -p /usr/bin/python2.7 venv
    Running virtualenv with interpreter /usr/bin/python2.7
    New python executable in venv/bin/python2.7
    Also creating executable in venv/bin/python
    Installing setuptools, pip...done.
    $ source venv/bin/activate


## Get source

    $ git clone https://bitbucket.org/simhay/html-json-products.git sainsburys

## Install dependencies from requirements.txt

    $ cd sainsburys
    $ pip install -r requirements.txt


# Testing
    
    $ make test
    py.test -v --cov-report term  --cov=/home/simon/sainsburys tests/
    ================================================ test session starts ================================================
    platform linux2 -- Python 2.7.6, pytest-2.9.1, py-1.4.31, pluggy-0.3.1 -- /home/simon/venv/bin/python2.7
    cachedir: .cache
    rootdir: /home/simon/sainsburys, inifile:
    plugins: cov-2.2.1
    collected 9 items

    tests/test_download.py::test_download_non_200 PASSED
    tests/test_download.py::test_download_200 PASSED
    tests/test_parse.py::TestListings::test_extract_product_listings PASSED
    tests/test_parse.py::TestProducts::test_extract_price PASSED
    tests/test_parse.py::TestProducts::test_extract_price_non_unit PASSED
    tests/test_parse.py::TestProducts::test_extract_price_non_spaces PASSED
    tests/test_parse.py::TestProducts::test_extract_product_detail PASSED
    tests/test_products.py::test_creation PASSED
    tests/test_products.py::test_size_in_kb PASSED
    ---------------------------------- coverage: platform linux2, python 2.7.6-final-0 ----------------------------------
    Name                     Stmts   Miss  Cover
    --------------------------------------------
    download.py                  7      0   100%
    main.py                     27     27     0%
    output.py                   10     10     0%
    parse.py                    20      0   100%
    products.py                  9      0   100%
    tests/__init__.py            0      0   100%
    tests/fixtures.py            9      0   100%
    tests/quick.py               6      6     0%
    tests/test_download.py      21      0   100%
    tests/test_parse.py         19      0   100%
    tests/test_products.py      12      0   100%
    --------------------------------------------
    TOTAL                      140     43    69%

    ============================================= 9 passed in 0.33 seconds ==============================================


# Running

## Place product listings urls one per line in urls.txt

    $ cat urls.txt
    http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html

## Run and JSON output to stdout

    $ python main.py < urls.txt 2> error.log
    {"total": "15.10", "results": [{"size": "38.27kb", "description": "Apricots", "unit_price": "3.50", "title": 
    "Sainsbury's Apricot Ripe & Ready x5"}, {"size": "38.67kb", "description": "Avocados", "unit_price": "1.50", 
    "title": "Sainsbury's Avocado Ripe & Ready XL Loose 300g"}, {"size": "43.44kb", "description": "Avocados", 
    "unit_price": "1.80", "title": "Sainsbury's Avocado, Ripe & Ready x2"}, {"size": "38.68kb", "description": 
    "Avocados", "unit_price": "3.20", "title": "Sainsbury's Avocados, Ripe & Ready x4"}, {"size": "38.54kb", 
    "description": "Conference", "unit_price": "1.50", "title": "Sainsbury's Conference Pears, Ripe & Ready x4 (minimum)"}, 
    {"size": "38.56kb", "description": "Gold Kiwi", "unit_price": "1.80", "title": "Sainsbury's Golden Kiwi x4"}, 
    {"size": "38.98kb", "description": "Kiwi", "unit_price": "1.80", "title": "Sainsbury's Kiwi Fruit, Ripe & Ready x4"}]}

